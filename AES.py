from cryptography.hazmat.primitives.ciphers.aead import AESGCM
from cryptography.exceptions import InvalidTag
import os


def aes_256_gcm_decrypt(nonce, key, ct_tag):
    """Decryption handling, decrypts the ct_tag parameter and returns the result decrypted as plaintext
    :param nonce: bytearreay of 12 bytes
    :param key: 256 bit length key
    :param ct_tag:
    :return: pt
    """
    aesgcm = AESGCM(key)
    pt = aesgcm.decrypt(nonce, ct_tag, None)
    return pt


def aes_256_gcm_encrypt(nonce, key, plain):
    """Encryption handling, encrypts given parameter plain and returns the encrypted plaintext.
    checks for equality of used nonce to ensure no reuse is made.
    :param nonce:
    :param key:
    :param plain:
    :return:
    """
    nonce_s = os.urandom(12)
    while nonce == nonce_s:
        nonce_s = os.urandom(12)

    aesgcm = AESGCM(key)
    ct = aesgcm.encrypt(nonce, plain, None)
    return ct


def generate_nonce():
    """Creates a random nonce
    :return: random nonce of 12 bytes length
    """
    return os.urandom(12)