import unittest
import AES


class AESTest(unittest.TestCase):
    """Test function to test AES"""

    def test_enc(self):
        """AES256GCM encryption Test function
        :return: nothing
        """
        #bytearray versions,
        key = b"yellow submarine"
        nonce = b"\x00"*12
        cipher = b"deadbeef"
        tag = b"\xff"

        print(len(key), key, nonce, cipher)

        # Ns = message.payload[0:12]
        # Cs = message.payload[13:34]
        # Ts = message.payload[35:51]

        ct = AES.aes_256_gcm_encrypt(nonce, key, cipher)
        cipher = ct[:-16]
        tag = ct[-16:]
        print(cipher, tag)
        print(cipher.hex(), tag.hex())
        self.assertEqual(tag, b'6N\xf9\xd4\x91p^\xb7\x84\xd2\x98I\x04\xe0\xa3\x1c')
        self.assertEqual(cipher, b'\x90\x8e\xc9\xc8j\xa0\x19)')

    def test_decrypt(self):
        """AES256GCM decryption test function
        :return: nothing
        """
        tag = b'6N\xf9\xd4\x91p^\xb7\x84\xd2\x98I\x04\xe0\xa3\x1c'
        cipher = b'\x90\x8e\xc9\xc8j\xa0\x19)'
        key = b"yellow submarine"
        nonce = b"\x00"*12

        plain = AES.aes_256_gcm_decrypt(nonce, key, cipher+tag)
        print(plain)
        self.assertEqual(plain, b"deadbeef")

if __name__ == '__main__':
    unittest.main()
