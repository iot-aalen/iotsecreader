from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric.x25519 import X25519PrivateKey
from cryptography.hazmat.primitives.asymmetric.x25519 import X25519PublicKey
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat
from cryptography.hazmat.primitives.serialization import load_der_public_key

class DH:
    """DH calculation functions."""
    def __init__(self):
        """Generate private key"""
        self.reader_keypair = X25519PrivateKey.generate()

    def calculate_x(self, pub_encoded):
        """calculate shared secret x"""
        # pub_encoded is in SubjectPublicKeyInfo format
        pub_decoded = load_der_public_key(pub_encoded, backend=default_backend())
        shared_x = self.reader_keypair.exchange(pub_decoded)
        return shared_x

    def get_encoded_pubkey(self):
        """Get the encoded public key"""
        pubkey = self.reader_keypair.public_key()
        # DER binary format, same as middle block from PEM
        return pubkey.public_bytes(encoding=Encoding.DER, format=PublicFormat.SubjectPublicKeyInfo)
        