import unittest
from DH import DH


class TestDH(unittest.TestCase):
    """Tests for DH  key exchange"""

    def test_gen_keypair(self):
        """Test of private key generating"""
        c = DH()
        self.assertIsNotNone(c.reader_keypair)
        # print(c.reader_keypair)

    def test_calc_x(self):
        """Test two different key creations. Get encoded public key and calculate the shared x. Test if both are the same"""
        c1 = DH()
        c2 = DH()
        c1_pub = c1.get_encoded_pubkey()
        c2_pub = c2.get_encoded_pubkey()
        # print(c1_pub)
        x1 = c1.calculate_x(c2_pub)
        x2 = c2.calculate_x(c1_pub)
        self.assertEqual(x1, x2)


if __name__ == '__main__':
    unittest.main()

