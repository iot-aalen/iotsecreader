import os
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.backends import default_backend

def HKDF_derive(key):
    """Derives keys of a fixed size
    :param key: key input to derive from
    :return: the derived key and the salt
    """
    salt = os.urandom(8)

    hkdf = HKDF(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        info=None,
        backend=default_backend()
    )
    return (salt, hkdf.derive(key))
