import unittest
from Derivation import HKDF_derive

from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.backends import default_backend


class TestDerivation(unittest.TestCase):
    """Test class for derivation purposes"""
    def test_HKDF(self):
        """HKDF test function. generate a key, derive and verify after that to ensure both works
        :return:
        """
        key = b'0*0\x05\x06\x03+en\x03!\x00\x93g1\xa7&i\xe3\xc2\x9b\x80Pe)\xbe\x88\x9d\x03\xe0\x10v\xf3\\\xf5f\x96\xd7Z7)PI\x12'
        salt, result = HKDF_derive(key)

        hkdf = HKDF(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            info=None,
            backend=default_backend()
        )
        hkdf.verify(key, result)


if __name__ == '__main__':
    unittest.main()
