from Transform import input_to_byte, generate_p, generate_message, generate_mi, generate_i, split_and_print, split_response
from DH import DH
from Derivation import HKDF_derive
from AES import aes_256_gcm_decrypt, aes_256_gcm_encrypt, generate_nonce, InvalidTag

def receive_from_reader(keypair: DH, data: bytes, uuid_reader: bytes, uuid_tag: bytes):
    """
    decrypt received message from the reader using given parameters
    :param keypair:
    :param data:
    :param uuid_reader: 16 Bytes length
    :param uuid_tag:
    :return: msg as the receivd encrypted message and k
    """
    pub_server = input_to_byte(data)
    print(pub_server.hex)
    x = keypair.calculate_x(pub_server)
    salt, k = HKDF_derive(x)
    
    nonce = generate_nonce()
    plain = generate_p(uuid_tag, uuid_reader)
    ct_tag = aes_256_gcm_encrypt(nonce, k, plain)
    m = generate_message(ct_tag, nonce, salt, k)
    i = generate_i(pub_server)
    msg = generate_mi(i, m)

    split_and_print(msg)
    return (msg, k)

def process_srv_response(resp: bytes, key_x: bytes):
    """Splits received response message from server and tries to decrypt it."""
    n, ct_tag = split_response(resp)
    try:
        plain = aes_256_gcm_decrypt(n, key_x, ct_tag)
        print('-> SERVER DATA:', plain)
    except InvalidTag:
        print('-> Error: Decryption failed!', n, ct_tag)