import decryption
class Test:
    #AES256GCM Decryption Test
    #bytearray versions,
    key = b"1100110011001100110011001100110011001100110011001100110011001100"
    nonce = b"110011001100110011001100"
    cipher = b"110011001100110011001100110011001100110011"
    tag = b"11001100110011001100110011001100"

    #String versions of the bytearrays
    k = key.decode("ascii")
    n = nonce.decode("ascii")
    c = cipher.decode("ascii")
    t = tag.decode("ascii")

    # Ns = message.payload[0:12]
    # Cs = message.payload[13:34]
    # Ts = message.payload[35:51]

    p = decryption.aes_256_gcm(nonce, key, cipher, tag)
    str1 = p.decode("ascii")
    print(str)
