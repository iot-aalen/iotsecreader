import unittest
from Transform import *

class TestDH(unittest.TestCase):
"""Testing DH key exchange by guessing a server key and other variables because of unavailability of the java server side"""
    def test_concat(self):

        # x = b'\x42'*32
        c = b'\x41'*21                                  # '7ed3064c4af5d60574b5e39849d20a3cb0eb2d17e5'
        t = b'\x42'*16                                            # '29933560347960a7585438877000ff3d'
        n = b'\x43'*12                                            # 'bb3b1fb2764e81d9e3556ae7'
        s = b'\x44'*8                   # '6b0aeadb1a3ec6b8'
        # kr= Key-Reader (public) | ks = Key-Server (public)
        kr = b'\x45'*32
        ks = b'\x46'*32

        print('c: ', c)
        print('t: ', t)
        print('n: ', n)
        print('s: ', s)
        message = generate_message(c, t, n, s, kr)    # reader_key['public']
        self.assertEqual(message, b'AAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBCCCCCCCCCCCCDDDDDDDDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE')
        # print(message)

        the_i = generate_i(ks)    # server_key['public']
        self.assertEqual(the_i, b'\xdd\xd3\xc9\xe6')

        final_message = generate_mi(the_i, message)
        # print(final_message)
        self.assertEqual(final_message, b'\xdd\xd3\xc9\xe6AAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBCCCCCCCCCCCCDDDDDDDDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE')


if __name__ == '__main__':
    unittest.main()
