import hashlib
import os
# from old.x25519 import generate_private_key


def generate_message(ct, n, s, k):
    """
    Generate the message from given parameters
    :param ct:
    :param n:
    :param s:
    :param k:
    :return: the combined message m
    """
    m = ct + n + s + k
    return m


def sha256(hashing_value):
    """Hashing the hshing value, in this case the server_public_key"""
    # txt = bytes(hashing_value, encoding='hex')
    # txt = hashing_value.encode('ascii')
    hash_object = hashlib.sha256(hashing_value)
    dig = hash_object.digest()
    print('sha: public-key: ', dig)
    return dig


def generate_i(server_public_key):
    """Generate i from the first 4 Bytes of the SHA256-Hashed Server Public Key"""
    sha256_skey = sha256(server_public_key)  # server_key['public']
    print('I before sliced:', sha256_skey)
    i = sha256_skey[:4]
    print('I: ', i)
    return i


def generate_mi(i, m):
    """Generate a new message using i"""
    # Concatenate I || M
    new_m = i + m
    return new_m


def input_to_byte(input):
    """
    Return given input to byte 'datatype'
    :param input:
    """
    return bytearray(input)


def generate_p(uid_token, uuid_reader):
    """Generates the plaintext"""
    return uid_token[:5] + uuid_reader[:16]


def split_and_print(message: bytes):
    """
    Splits and prints the message
    :param message:
    :return:
    """
    # i: 4 | c: 21 | t: 16 | n: 12 | s: 8 | k: 32
    print("(+) printing splitted message: ")
    print('i (hashed S-Pub-Key): ', message[:4].hex())
    print('cipher : ', message[4:25].hex())
    print('tag : ', message[25:41].hex())
    print('nonce: ', message[41:53].hex())
    print('salt: ', message[53:61].hex())
    print('Public-Key-Reader: ', message[61:93].hex())

def split_response(msg):
    """Split the response from the server into 'Nonce' and 'Ciphertext + Tag'"""
    return (
        msg[:12], # Nonce
        msg[12:] # Ciphertext + Tag
    )