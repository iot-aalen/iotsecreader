# Mqtt Reader. Read RFID tags and broadcast UID via MQTT. This is educational software.
# Copyright (C) 2019 Marcus Gelderie
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import asyncio
import logging
import sys
from typing import Tuple, Optional
import paho.mqtt.client as mqttclient  # type: ignore

# pylint: disable=logging-fstring-interpolation

logger = logging.getLogger(__name__)


class MQTTClient:
    def __init__(self):
        self._client = mqttclient.Client()
        self._canceled = False
        self._pending_messages = {}
        self._connected_future = asyncio.get_running_loop().create_future()
        self._publish_futures = {}
        self._subscribe_futures = {}
        self._client.on_publish = self._on_publish
        self._client.on_connect = self._on_connect
        self._client.on_subscribe = self._on_subscribe
        self._client.on_message = self._on_message

    def connect(self, *args, **kwargs):
        # This function blocks, but it does not wait for the broker to acknowledge
        logger.info(f'Connecting to broker...')
        self._client.connect(*args, **kwargs)
        logger.info(f'Connected.')

    async def connected(self) -> Tuple[dict, int]:
        if self._connected_future.done():
            res = self._connected_future.result()
        else:
            res = await self._connected_future
        logger.info(f'Connection ACKed by broker.')
        self._connected_future = asyncio.get_running_loop().create_future()
        return res

    def publish(self, topic, message, *args, **kwargs) -> Optional[int]:
        logger.debug('Publishing message to topic \'%s\'.', topic)
        message_info = self._client.publish(topic, message, *args, **kwargs)
        if message_info.rc != mqttclient.MQTT_ERR_SUCCESS:
            logger.error('Could not publish message. Reason: [%d] %s',
                         message_info.rc,
                         mqttclient.error_string(message_info.rc))
            return None
        logger.debug('Published message to topic \'%s\'.', topic)
        if not message_info.mid in self._publish_futures:
            self._publish_futures[
                message_info.mid] = asyncio.get_running_loop().create_future()
            logger.debug('Created new future for message ID: %d',
                         message_info.mid)
        return message_info.mid

    async def published(self, message_id) -> Optional[int]:
        try:
            received_message_id = await self._publish_futures[message_id]
            del self._publish_futures[message_id]
        except KeyError:
            logger.error(
                'Was told to wait for publish ACK for message %d, but there was no future for it.',
                message_id)
            return None
        if received_message_id != message_id:
            logger.error('Received %d, but was looking for %d.',
                         received_message_id, message_id)
            return None
        return message_id

    def subscribe(self, topic: str, *args, **kwargs) -> Optional[int]:
        logger.info(f'Subscribing to topic \'{topic}\'')
        if self._pending_messages.get(topic) is not None:
            logger.warning(
                f'Attempting to subscribe to {topic}, but there\'s already a queue for it.'
            )
        else:
            self._pending_messages[topic] = asyncio.Queue()

        logger.debug(f'Enqueuing subscription for {topic}')
        result, message_id = self._client.subscribe(topic, *args, **kwargs)
        if result != mqttclient.MQTT_ERR_SUCCESS:
            logger.error('Got error code during subscribe: [%d] %s', result,
                         mqttclient.error_string(result))
            return None
        self._subscribe_futures[message_id] = asyncio.get_running_loop(
        ).create_future()
        logger.debug(f'Message ID for subscription was: {message_id}')
        return message_id

    async def subscribed(self, message_id) -> Optional[int]:
        try:
            return await self._subscribe_futures[message_id]
        except KeyError:
            logger.error(
                'Was told to wait for subscribed ACK for message %d,'
                'but there was no future for it', message_id)
            return None

    async def run(self):
        while not self._canceled:
            self._client.loop()
            await asyncio.sleep(0.5)

    async def receive(self, topic):
        return await self._pending_messages[topic].get()

    def cancel(self):
        self._canceled = True

    def _on_connect(self, client, userdata, flags: dict,
                    returncode: int) -> None:
        logger.debug(
            f'Connect callback called with returncode: {mqttclient.connack_string(returncode)}'
        )
        del client, userdata  # unused
        self._connected_future.set_result([flags, returncode])

    def _on_publish(self, client, userdata, message_id):
        logger.debug(f'Publish ACK received with message_id {message_id}')
        del client, userdata  # unused
        try:
            future = self._publish_futures[message_id]
        except KeyError:
            logger.error(
                'Received publish ACK for message ID %d, but there was no future for it.',
                message_id)
            logger.debug('Creating a future for it now')
            future = asyncio.get_running_loop().create_future()
            self._publish_futures[message_id] = future
        future.set_result(message_id)

    def _on_message(self, client, userdata, message):
        logger.debug(f'Message callback called. Topic: {message.topic}')
        del client, userdata  # unused
        self._pending_messages[message.topic].put_nowait(message)

    def _on_subscribe(self, client, userdata, message_id, granted_qos):
        logger.debug(f'Subscribe acknowledge: {message_id}')
        del client, userdata, granted_qos  # unused
        try:
            self._subscribe_futures[message_id].set_result(message_id)
        except KeyError:
            logger.error(
                'Received subscribe ACK for message ID %d, but there was no future for it.',
                message_id)


async def receive_loop(client, topic):
    while True:
        message = await client.receive(topic)
        if message:
            print(message.topic, ':', message.payload.decode("ascii"))
        else:
            print('Empty message')


async def main():
    nargs = len(sys.argv)
    if nargs >= 2:
        host = sys.argv[1]
    else:
        host = 'localhost'
    if nargs >= 3:
        port = int(sys.argv[2])
    else:
        port = 1883

    client = MQTTClient()
    forever = asyncio.create_task(client.run())
    logger.info(f'Connecting to "{host}" on port "{port}"')
    client.connect(host, port)
    await client.connected()
    message_id = client.subscribe('interesting')
    logger.info('Subscribed to topic \'interesing\'. Waiting for ACK...')
    await client.subscribed(message_id)
    receive = asyncio.create_task(receive_loop(client, 'interesting'))
    message_id = client.publish(topic='foo', message=b'blablabla', qos=2)
    logger.info('Published message. Waiting for ACK...')
    await client.published(message_id)
    logger.info('Received ACK.')

    await asyncio.gather(receive, forever)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    asyncio.run(main())
