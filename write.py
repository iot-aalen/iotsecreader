from DH import DH
import rfid
import time

pair = DH()
reader = rfid.Reader()
tag = reader.get_card()
while not tag:
    time.sleep(0.3)
    tag = reader.get_card()
    print('Sleeping')

data = list(pair.get_encoded_pubkey())
print(len(data), pair.get_encoded_pubkey().hex())
tag.write(0x01, data)